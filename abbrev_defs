;;-*-coding: utf-8;-*-
(define-abbrev-table 'global-abbrev-table
  '(
    ("ae" "ä" nil :count 3)
    ("oe" "ö" nil :count 2)
    ("ss" "ß" nil :count 2)
    ("ue" "ü" nil :count 1)
   ))

