(require 'package) 
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; set type of line numbering (global variable)
(setq display-line-numbers-type 'relative) 

;; activate line numbering in all buffers/modes
(global-display-line-numbers-mode)
;; (set-face-attribute 'default nil :height 155)
(setq latex t
      org t
      mac nil)
(setq ns-alternate-modifier 'none)

(setq TeX-command-list
      '(("MakeMake" "make -k" TeX-run-TeX nil t :help "Mein eigener Kompilierbefehl")))

(if (eq org t)
    (load-file "~/.emacs.d/toobi/org.el")
  (message "Org config wurde nicht geladen."))

(if (eq mac t)
    (load-file "~/.emacs.d/toobi/mac.el")
  (message "Mac config wurde nicht geladen."))

(defun as/open-studium ()
  (interactive)
  (dired "~/Desktop/studium/6-semester"))

(defun as/open-init ()
  (interactive)
  (find-file "~/.emacs.d/init.el"))

(use-package gruber-darker-theme
  :config (load-theme 'gruber-darker t))

(use-package all-the-icons)

(defun as/set-font ()
  (interactive)
  (set-face-attribute 'default nil :height 130))

(use-package tex
  :ensure auctex
  :config
  (setq TeX-auto-save t ;; Speichert automatisch style informationen
	TeX-parse-self t ;; 

	;; Setzt automatisch passende Klammern falls man mit einer linken Anfaengt
	LaTeX-electric-left-right-brace t
	;; Wenn man das nicht moechte, C-u 1 bevor man Klammer macht
	))

(setq warning-minimum-level :error)

(use-package evil
  :init
  (setq evil-want-integration t
	evil-want-keybinding nil
	evil-want-C-u t
	evil-want-C-i-jump nil)

  :config
  (evil-mode t)
  ;; Stadien wo ich gerne den normalen Emacs Mode haben wuerde
  (dolist (state '(speedbar-mode compilation-mode
		   dired-mode xref--xref-buffer-mode
		   shell-mode eshell
		   vterm-mode term-mode
		   shell docker-container-mode
		   docker-image-mode docker-network-mode
		   docker-volume-mode Buffer-menu-mode
		   ebib-index-mode ebib-strings-mode
		   ebib-entry-mode ebib-log-mode
		   ebib-multiline-mode))
    (evil-set-initial-state state 'emacs))
  ;; Globale Shortcuts

  (evil-define-key 'normal 'global
    ",C" 'recompile
    ",c" 'compile
    ",D" 'projectile-run-gdb
    ",d" 'gdb
    ",f" 'projectile-find-file-dwim
    ",g" 'magit-status
    ",b" 'projectile-switch-to-buffer
    " f" 'find-file)

  (evil-define-key 'visual 'global
    ",r" 'replace-string))

(use-package evil-collection
  :config
  (evil-collection-init '(vterm dired ebib ibuffer help)))

(use-package browse-kill-ring)

(add-hook 'python-mode-hook
	  (lambda () (setq indent-tabs-mode t)))

(use-package popper
  :init
  (evil-define-key '(normal insert) 'global
    (kbd "M-,") 'popper-toggle-latest
    (kbd "C-,") 'popper-cycle
    (kbd "C-M-,") 'popper-toggle-type)

  (popper-mode +1)
  (popper-echo-mode +1))

(use-package vterm)

(evil-define-key 'normal 'global " o" 'other-window)

(use-package bm
  :config
  (evil-define-key 'normal 'global
    " mm" 'bm-toggle
    " mj" 'bm-next
    " mk" 'bm-previous))

(use-package embark
  :bind
  (("C-c o" . embark-act)
   ("C-c e" . embark-dwim)))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
	 ("C-x b" . counsel-switch-buffer)
	 ("C-x C-f" . counsel-find-file)))

(use-package perspective
 :init (persp-mode)
 :bind ("C-x b" . persp-ivy-switch-buffer)
 :custom
 (persp-mode-prefix-key (kbd "C-c M-p")) 
 :config
 (evil-define-key 'normal 'global
   " b" 'persp-ivy-switch-buffer
   " B" 'ibuffer))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode)

(use-package nyan-mode
  :init 
  (nyan-mode)
  :config 
  (nyan-start-animation)
  (nyan-toggle-wavy-trail))

(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :hook (dired-mode-hook . evil-mode)
  :bind (:map dired-mode-map
	      ("j" . dired-next-line)
	      ("k" . dired-previous-line)
	      ("h" . dired-up-directory)
	      ("l" . dired-find-file)
	      ("J" . dired-goto-file)
	      (")" . dired-hide-dotfiles-mode))
  :custom ((dired-listing-switches "-agho")))

(use-package dired-hide-dotfiles
  :hook (dired-mode . dired-hide-dotfiles-mode))

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package highlight-indent-guides
  :init
  (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
  (setq highlight-indent-guides-method 'character))

(use-package magit)

(use-package avy
  :config
  (evil-define-key 'normal 'global ",:" 'avy-goto-char))

(dolist (mode '(term-mode-hook
		shell-mode-hook
		vterm-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda ()
		   (display-line-numbers-mode 0)
		   (hl-line-mode 0))))

(use-package smartparens
  :hook (prog-mode . smartparens-mode)
  :config
  (evil-define-key 'visual prog-mode-map
    ",(" 'sp-wrap-round
    ",{" 'sp-wrap-curly
    ",[" 'sp-wrap-square)

  (evil-define-key 'insert prog-mode-map
    (kbd "C-c C-a") 'sp-beginning-of-sexp
    (kbd "C-c C-e") 'sp-end-of-sexp
    (kbd "C-c a") 'sp-backward-up-sexp
    (kbd "C-c e") 'sp-up-sexp)

  (evil-define-key 'normal prog-mode-map
    (kbd "C-c C-r") 'sp-rewrap-sexp
    (kbd "C-c C-k") 'sp-splice-sexp
    (kbd "C-c C-l") 'sp-forward-slurp-sexp
    (kbd "C-c C-h") 'sp-forward-blarf-sexp
    (kbd "C-c C-c") 'comment-or-uncomment-region
    " me" 'sp-end-of-sexp
    " mE" 'sp-up-sexp
    " ma" 'sp-beginning-of-sexp
    " mA" 'sp-backward-up-sexp
    " mn" 'sp-down-sexp
    " ml" 'sp-forward-sexp
    " mh" 'sp-backward-sexp
    " rw" 'sp-rewrap-sexp
    " rk" 'sp-splice-sexp
    " sl" 'sp-forward-slurp-sexp
    " sh" 'sp-forward-barf-sexp
    " sH" 'sp-backward-slurp-sexp
    " sL" 'sp-backward-blarf-sexp
    ",," 'cider-eval-defun-at-point))

(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(add-hook 'prog-mode-hook 'hl-line-mode)
(fset 'yes-or-no-p 'y-or-n-p)

(use-package ivy-rich
  :init (ivy-rich-mode 1)
  :after counsel
  :config
  (setq ivy-format-function #'ivy-format-function-line))

;; Globale Shortcuts
(global-set-key (kbd "C-x k") 'kill-this-buffer)

;; Generelle Sachen
(setq inhibit-splash-screen t
      cursor-type 'bar
      visible-bell t
      mouse-wheel-scroll-amount '(3 ((shift) . 3)))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package ivy
  :diminish
  :bind (:map ivy-minibuffer-map
	      ("TAB" . ivy-alt-done)
	      ("C-j" . ivy-next-line)
	      ("C-l" . ivy-alt-done)
	      ("C-k" . ivy-previous-line)
	 :map ivy-switch-buffer-map
	      ("C-k" . ivy-previous-line)
	      ("C-d" . ivy-switch-buffer-kill))
  :init
  (ivy-mode))

(use-package swiper
  :init
  (global-set-key (kbd "C-s") 'swiper))

(use-package drag-stuff
  :hook (text-mode . drag-stuff-mode)
  :config
  (evil-define-key 'normal 'global
    (kbd "C-k") 'drag-stuff-up
    (kbd "C-j") 'electric-newline-and-maybe-indent))

;; Cpp Stuff
(use-package yasnippet
  :init (yas-global-mode 1)
  :config
  (setq yas-snippet-dirs '("~/.emacs.d/snippets/"))
  (evil-define-key 'insert 'global (kbd "C-SPC") 'yas-expand)
  (global-set-key (kbd "C-c C-y") 'yas-insert-snippet))

(use-package yasnippet-snippets)

(use-package yaml-mode)

(if (eq latex t)
    (load-file "~/.emacs.d/toobi/latex.el")
  (message "Latex wurde nicht geladen."))
