;; Falls Macbook, dann anderes Theme + mac-spezifische Sachen
(if (string= system-type "darwin")
    (progn
      (set-face-attribute 'default nil :height 150)
      (setq mac-option-key-is-meta nil
	    mac-option-modifier 'control 
	    mac-command-key-is-meta t
	    mac-command-modifier 'meta)
      (setenv "PATH" "/Users/alex/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/opt/homebrew/opt/openssl@3/bin:/opt/homebrew/opt/libpq/bin:/Users/alex/.nvm/versions/node/v16.15.0/bin:/opt/homebrew/opt/openjdk/bin:/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/TeX/texbin:/opt/X11/bin:/Library/Apple/usr/bin:/Users/alex/.cargo/bin")))

