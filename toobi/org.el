;; Configs fuer Orgmode
(global-set-key (kbd "C-c a") 'org-agenda)

;; Orgmode Timetracking
(global-set-key (kbd "C-c C-x C-j") 'org-clock-goto)
(global-set-key (kbd "C-c C-x C-o") 'org-clock-out)
(global-set-key (kbd "C-c C-x C-x") 'org-clock-in-last)

(setq org-babel-python-command "python3")

;; (setq org-clock-persist 'history)
;; (org-clock-persistence-insinuate)

(add-hook 'text-mode-hook 'display-fill-column-indicator-mode)
(add-hook 'text-mode-hook 'auto-fill-mode)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (python . t)
   (C . t)))

(setq org-todo-keywords
      '((sequence "TODO" "IN ARBEIT" "|" "DONE")))

(use-package org-bullets
  :config (add-hook 'org-mode-hook (lambda()
				     (org-bullets-mode 1))))

(use-package org-roam
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/.org-roam")
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert)
	 ("C-c n j" . org-roam-dailies-capture-today)
	 ("C-c n c" . org-roam-capture)
	 :map org-mode-map
	 ("C-M-i" . completion-at-point)) 
  :config
  (org-roam-setup))
