(message "Hello aus der Latex Funktion")

(setq org-latex-caption-above nil)

(setq ebib-bibtex-dialect 'biblatex)


(use-package flyspell
  :config
  (add-hook 'text-mode-hook 'flyspell-mode))


(use-package org-ref
  :config
  (define-key org-mode-map (kbd "C-c ]") 'org-ref-insert-link)
  (evil-define-key 'normal 'bibtex-mode-map (kbd "M-b") 'org-ref-bibtex-hydra/body))

(with-eval-after-load "ox-latex"
  (add-to-list 'org-latex-classes
               '("scrreprt" "\\documentclass{scrreprt}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(use-package ivy-bibtex
  :init
  (setq bibtex-completion-bibliography '("~/Desktop/wissArbeit/notes/all.bib")
	bibtex-completion-library-path '("~/wiss/pdfs")
	bibtex-completion-notes-path "~/Desktop/wissArbeit/notes"))

(setq org-latex-pdf-process
      '("pdflatex -interaction nonstopmode -output-directory %o %f"
	"bibtex %b"
	"pdflatex -interaction nonstopmode -output-directory %o %f"
	"pdflatex -interaction nonstopmode -output-directory %o %f"))

(use-package ebib
  :config
  (evil-define-key 'normal 'global " l" 'ebib)
  (setq ebib-notes-directory "~/pnotes")
  (setq ebib-file-associations '(("pdf" . "open") ("ps" . "gv"))))
