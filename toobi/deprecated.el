;; Enthaelt Pakete die zurzeit nicht verwendet werden,
;; aber evtl. trotzdem Interessant sind.

(use-package flycheck)

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-position 'bottom))

(use-package company
  :bind (:map company-active-map
	      ("RET" . company-complete-selection)
	      ("C-SPC" . keyboard-quite))
  :config 
  (evil-define-key 'insert 'global (kbd "C-SPC") 'company-indent-or-complete-common)
  (setq company-idle-delay 1)
  (setq company-show-numbers t))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package docker
  :bind ("C-c d" . docker))

(use-package elpy)

(use-package python-mode
  :custom
  (python-shell-interpreter "python3"))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook
  (lsp-mode . lsp-mode-setup)
  (lsp-mode . global-flycheck-mode)
  (lsp-mode . yas-minor-mode-on)
  (java-mode . lsp-deferred)
  (c++-mode . lsp-deferred)
  (c-mode . lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")

  :config
  (lsp-enable-which-key-integration t)
  (setq lsp-ui-sideline-enable nil
        lsp-ui-sideline-show-code-actions nil
        lsp-signature-render-documentation nil)
  (setq lsp-completion-provider :none)
  (lsp-register-client
   (make-lsp-client :new-connection (lsp-tramp-connection "clangd")
		    :major-modes '(c-mode c++-mode)
		    :remote? t
		    :server-id 'clangd-remote)))
